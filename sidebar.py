from tkinter import *
from tkmacosx import Button
from PIL import ImageTk, Image

class Sidebar(Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.configure(bg="#5b97ca", width=500)

        # Example sidebar content
        self.label = Label(self, text=" Real-Time Bus Passenger Counting ", bg="#5b97ca", fg="white", font=("Palatino", 15))
        self.label.pack(pady=10)

        # Get the dimensions of the label
        label_width = self.label.winfo_reqwidth()

        # Create a canvas to draw the line
        self.line_canvas = Canvas(self, bg="lightslategrey", highlightthickness=0, width=label_width, height=2)
        self.line_canvas.pack(fill="x", padx=10)

        # Draw a line at the top of the label
        self.line_canvas.create_line(0, 2, label_width, 2, fill="white", width=2)

        # Load icons
        realtime_count_icon = Image.open("eye.png")  # Updated icon name
        playback_icon = Image.open("record.png")

        # Resize icons as needed
        icon_size = (20, 20)
        realtime_count_icon = realtime_count_icon.resize(icon_size)  # Updated icon name
        playback_icon = playback_icon.resize(icon_size)

        # Convert icons to PhotoImage objects
        self.realtime_count_icon_default = ImageTk.PhotoImage(realtime_count_icon)  # Updated icon name
        self.playback_icon_default = ImageTk.PhotoImage(playback_icon)

        # Load white icons
        realtime_count_icon_white = Image.open("eye_white.png")  # Updated icon name
        playback_icon_white = Image.open("record_white.png")

        # Resize white icons
        realtime_count_icon_white = realtime_count_icon_white.resize(icon_size)  # Updated icon name
        playback_icon_white = playback_icon_white.resize(icon_size)

        # Convert white icons to PhotoImage objects
        self.realtime_count_icon_white = ImageTk.PhotoImage(realtime_count_icon_white)  # Updated icon name
        self.playback_icon_white = ImageTk.PhotoImage(playback_icon_white)

        # Create buttons for each page
        self.realtime_count_button = Button(self, text=" Real-Time Count      ", image=self.realtime_count_icon_default, compound="left", command=lambda: self.on_button_clicked("Real-Time Count"), bg="lightslategrey", fg="#193750", font=("Segoe UI", 20), borderless=True, anchor="w")  # Updated button text and icon
        self.realtime_count_button.pack(fill="x", padx=(0, 0), pady=(10, 0))

        self.playback_button = Button(self, text=" Playback                    ", image=self.playback_icon_default, compound="left", command=lambda: self.on_button_clicked("Playback"), bg="lightslategrey", fg="#193750", font=("Segoe UI", 20), borderless=True, anchor="w")
        self.playback_button.pack(fill="x", padx=(0, 0))

        # Variable to store selected item
        self.selected_item = None

        # Call update_button_color to initialize button color
        self.update_button_color()

    def on_button_clicked(self, page_name):
        self.selected_item = page_name
        self.master.show_page(page_name)
        self.update_button_color()

    def update_button_color(self):
        # Reset all buttons to default color
        self.realtime_count_button.configure(bg="#80afd6", fg="#193750", image=self.realtime_count_icon_default)
        self.playback_button.configure(bg="#80afd6", fg="#193750", image=self.playback_icon_default)

        # Highlight the selected button
        if self.selected_item == "Real-Time Count":
            self.realtime_count_button.configure(bg="dodgerblue", fg="white", image=self.realtime_count_icon_white)
        elif self.selected_item == "Playback":
            self.playback_button.configure(bg="dodgerblue", fg="white", image=self.playback_icon_white)

    def get_selected_item(self):
        return self.selected_item
