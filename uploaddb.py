import cv2
import firebase_admin
from firebase_admin import credentials, db
import base64

# Firebase credentials and initialization
cred = credentials.Certificate("patpat-a2258-firebase-adminsdk-sfbtb-1705d584a0.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://patpat-a2258-default-rtdb.asia-southeast1.firebasedatabase.app/'
})

# Create a VideoCapture object
cap = cv2.VideoCapture(0)  # Use 0 for default webcam, or pass the path to a video file

# Check if the camera/video stream is opened successfully
if not cap.isOpened():
    print("Error: Could not open video stream")
    exit()

# Reference to Firebase RTDB path
ref = db.reference("/Video/Stream")

# Function to continuously read and send frames
def send_frames():
    while True:
        # Read a frame from the video stream
        ret, frame = cap.read()

        # Check if the frame is successfully read
        if not ret:
            print("Error: Could not read frame")
            break

        # Convert the frame to base64 encoded string
        _, buffer = cv2.imencode('.png', frame)  # Encode as PNG instead of JPEG
        png_as_text = base64.b64encode(buffer)

        # Upload the frame to Firebase RTDB
        ref.set(png_as_text.decode('utf-8'))

        # Print a message indicating frame upload
        print("Frame uploaded to Firebase RTDB")

# Start the frame uploading process
send_frames()
