import cv2
import numpy as np
import pandas as pd
from ultralytics import YOLO
from tracker import Tracker

model = YOLO('best.pt')

class_list = []
with open("coco.txt", "r") as my_file:
    data = my_file.read()
    class_list = data.split("\n")

area2 = [(690, 400), (690, 600), (730, 600), (730, 400)]
area1 = [(760, 400), (760, 600), (800, 600), (800, 400)]

entering = set()
people_entering = {}
tracker = Tracker()

def reset():
    global entering
    entering = set()

def detect_people(frame, current_count):
    global entering

    frame = cv2.resize(frame, (1024, 576))
    results = model.predict(frame)
    a = results[0].boxes.data
    px = pd.DataFrame(a).astype("float")

    list = []
    for index, row in px.iterrows():
        x1 = int(row[0])
        y1 = int(row[1])
        x2 = int(row[2])
        y2 = int(row[3])
        d = int(row[5])
        c = class_list[d]
        if 'Passenger' in c:
            list.append([x1, y1, x2, y2])

    bbox_id = tracker.update(list)
    for bbox in bbox_id:
        x3, y3, x4, y4, id = bbox
        results = cv2.pointPolygonTest(np.array(area2, np.int32), ((x4, y4)), False)
        if results >= 0:
            people_entering[id] = (x4, y4)
            cv2.rectangle(frame, (x3, y3), (x4, y4), (0, 0, 255), 2)
        if id in people_entering:
            results1 = cv2.pointPolygonTest(np.array(area1, np.int32), ((x4, y4)), False)
            if results1 >= 0:
                cv2.rectangle(frame, (x3, y3), (x4, y4), (0, 255, 0), 2)
                cv2.circle(frame, (x4, y4), 5, (255, 0, 255), -1)
                cv2.putText(frame, str(id), (x3, y4), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1)
                entering.add(id)

    cv2.polylines(frame, [np.array(area1, np.int32)], True, (255, 0, 0), 2)

    cv2.polylines(frame, [np.array(area2, np.int32)], True, (255, 0, 0), 2)

    p = len(entering)
    cv2.putText(frame, str(p), (60, 80), cv2.FONT_HERSHEY_COMPLEX, 0.7, (0, 0, 255), 2)

    return frame, p
