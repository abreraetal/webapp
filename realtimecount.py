import tkinter as tk
import firebase_admin
from firebase_admin import credentials, db
from tkmacosx import Button  # Import Button from tkmacosx for macOS-style buttons

# Firebase credentials and initialization
cred = credentials.Certificate("patpat-a2258-firebase-adminsdk-sfbtb-1705d584a0.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://patpat-a2258-default-rtdb.asia-southeast1.firebasedatabase.app/'
})

# Reference to Firebase RTDB path
ref = db.reference("/Live/Count")

class RealTimeCountPage(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.configure(bg="#e3edf6")
        self.label = tk.Label(self, text="Real-Time Count", bg="#e3edf6", fg="black", font=("Verdana", 18))
        self.label.pack(pady=10)

        # Create a Label widget to display current count
        self.count_label = tk.Label(self, text="", bg="#e3edf6", fg="darkblue", font=("Palatino", 45))
        self.count_label.pack(pady=10, padx=10, anchor=tk.CENTER)

        # Create a Label widget to display final count
        self.final_count_label = tk.Label(self, text="", bg="#e3edf6", fg="#52a736", font=("Palatino", 25))
        self.final_count_label.pack(pady=10, padx=10, anchor=tk.CENTER)

        # Initialize counts
        self.count = 0
        self.final_count = 0

        # Create a reset button with color
        self.reset_button = Button(self, text="Reset Count", command=self.reset_counts, bg="orange", fg="white", font=("Verdana", 15), borderless=True)
        self.reset_button.pack(pady=10)

        # Call fetch_count periodically
        self.fetch_count()

    def fetch_count(self):
        try:
            # Fetch count from Firebase RTDB
            new_count = ref.child('people_entering_count').get()

            if new_count is not None:
                # Update the count display
                self.update_count_display(new_count)
        except Exception as e:
            print("Error fetching count from Firebase:", e)

        # Schedule the next fetch after 1 second (adjust as needed)
        self.after(1000, self.fetch_count)

    def update_count_display(self, new_count):
        # Only accumulate counts received from Firebase
        if new_count > self.count:
            self.final_count += new_count - self.count

        # Update count display
        self.count = new_count
        self.count_label.config(text=f"Count: {self.count}")

        # Update final count display
        self.final_count_label.config(text=f"Final Count: {self.final_count}")

    def reset_counts(self):
        # Reset both counts to zero
        self.count = 0
        self.final_count = 0
        self.count_label.config(text="Count: 0")
        self.final_count_label.config(text="Final Count: 0")

        # Also reset count on Firebase
        ref.update({'people_entering_count': 0})

# The main function for testing the RealTimeCountPage
def main():
    root = tk.Tk()
    root.title("Real-Time Count")
    root.geometry("400x300")  # Adjusted window height for the reset button

    realtime_count_page = RealTimeCountPage(root)
    realtime_count_page.pack(fill=tk.BOTH, expand=True)

    root.mainloop()

# Check if the file is run directly and call the main function
if __name__ == "__main__":
    main()
