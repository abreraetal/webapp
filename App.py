import tkinter as tk
from navbar import Navbar
from sidebar import Sidebar
from realtimecount import RealTimeCountPage  # Updated import
from playback import PlaybackPage

class MainApplication(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set window title
        self.title("Cuenta")

        # Set window icon
        self.iconphoto(True, tk.PhotoImage(file="cuenta.png"))

        # Create navbar
        self.navbar = Navbar(self)
        self.navbar.pack(side="top", fill="x")

        # Create sidebar
        self.sidebar = Sidebar(self)
        self.sidebar.pack(side="left", fill="y")

        # Create container for pages
        self.page_container = tk.Frame(self, bg="#e3edf6")
        self.page_container.pack(side="right", fill="both", expand=True)

        # Initialize pages
        self.realtime_count_page = RealTimeCountPage(self.page_container)  # Updated instantiation
        self.playback_page = PlaybackPage(self.page_container)

        self.show_page("Real-Time Count")  # Updated initial page name

        # Bind sidebar item selection to show_page method
        self.sidebar.bind("<Double-1>", self.on_sidebar_item_selected)

    def on_sidebar_item_selected(self, event):
        selected_item = self.sidebar.get_selected_item()
        if selected_item == "Playback":
            self.realtime_count_page.stop_realtimecount_processing()
        elif selected_item == "Real-Time Count":
            self.playback_page.stop_playback_processing()
        self.show_page(selected_item)

    def show_page(self, page_name):
        # Hide all pages
        for page in [self.realtime_count_page, self.playback_page]:
            page.pack_forget()

        if page_name == "Real-Time Count":  # Updated page name
            self.realtime_count_page.pack(fill="both", expand=True)
        elif page_name == "Playback":
            self.playback_page.pack(fill="both", expand=True)

if __name__ == "__main__":
    app = MainApplication()
    app.geometry("1420x800")
    app.mainloop()
