import tkinter as tk
from PIL import Image, ImageTk

class Navbar(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.configure(bg="#132a3d", height=50)  # Set the background color and height of the navbar

        # Load the logo image and resize while maintaining aspect ratio
        logo_image = Image.open("name_white.png")  # Replace "name_white.png" with the path to your PNG image
        logo_image.thumbnail((150, 150))  # Resize the image to fit within 40x40 while maintaining aspect ratio
        self.logo_photo = ImageTk.PhotoImage(logo_image)

        # Create a label to display the logo
        self.logo_label = tk.Label(self, image=self.logo_photo, bg="#132a3d")
        self.logo_label.image = self.logo_photo  # Keep a reference to avoid garbage collection
        self.logo_label.pack(side="left", padx=10, pady=5)  # Adjust padding as needed

        # Create a label to display the title in the navbar
        self.title_label = tk.Label(self, text="© Abrera et. al.", fg="#c4d9ec", bg="#132a3d", font=("Roboto", 10))
        self.title_label.pack(side="left", padx=(0, 0), pady=(12, 0))



# Test the navbar independently if needed
if __name__ == "__main__":
    root = tk.Tk()
    root.title("Navbar Test")
    navbar = Navbar(root)
    navbar.pack(fill="x")
    root.mainloop()
