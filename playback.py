import tkinter as tk
from tkinter import filedialog
import cv2
from PIL import Image, ImageTk
import peoplecounter
from tkmacosx import Button

class PlaybackPage(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.config(bg="#e3edf6")

        self.label = tk.Label(self, text="Instant Playback", bg="#e3edf6", fg="black", font=("Verdana", 18))
        self.label.pack(pady=10)

        button_width = 110
        button_height = 30

        button_frame = tk.Frame(self, bg="#e3edf6")
        button_frame.pack(pady=(10, 0))

        # Label to display the count
        self.count_label = tk.Label(button_frame, text="Final Count: 0", bg="#e3edf6", fg="#52a736", font=("Verdana", 15))
        self.count_label.pack(side=tk.LEFT, padx=(0, 10), pady=(0, 20))

        self.reset_button = Button(button_frame, text="Reset Count", command=self.reset_count, bg="orange", fg="white", font=("Verdana", 13), borderless=True, width=button_width, height=button_height)
        self.reset_button.pack(side=tk.LEFT, padx=(0, 10), pady=(0, 20))

        self.upload_button = Button(button_frame, text="Upload Video", command=self.upload_video, bg="dodgerblue", fg="white", font=("Verdana", 13), borderless=True, width=button_width, height=button_height)
        self.upload_button.pack(side=tk.LEFT, padx=(0, 10), pady=(0, 20))

        self.stop_button = Button(button_frame, text="Stop", command=self.stop_playback_processing, bg="red", fg="white", font=("Verdana", 15), borderless=True, width=button_width, height=button_height)
        self.stop_button.pack(side=tk.RIGHT, padx=(10, 0), pady=(0, 20))


        self.processed_frame_label = tk.Label(self)
        self.processed_frame_label.pack()

        self.cap = None
        self.paused = False
        self.final_count = 0
        self.people_counter_count = 0
        self.display_count = 0
        self.total_final_count = 0  # New variable to track cumulative final count

    def upload_video(self):
        filepath = filedialog.askopenfilename()

        if filepath:
            cap = cv2.VideoCapture(filepath)

            if not cap.isOpened():
                print("Error: Failed to open video file.")
                return

            self.cap = cap
            self.paused = False

            # Update display_count with total_final_count
            self.display_count = self.total_final_count

            self.final_count = self.people_counter_count

            # Update total_final_count with the final_count from the current video
            self.total_final_count += self.final_count

            peoplecounter.reset()
            self.display_processed_frame()

    def stop_playback_processing(self):
        if self.cap and self.cap.isOpened():
            self.cap.release()
        self.cap = None
        self.paused = True

    def reset_count(self):
        # Reset count variables
        peoplecounter.reset()
        self.final_count = 0
        self.people_counter_count = 0
        self.total_final_count = 0

        # Update the count label to display 0
        self.count_label.config(text="Final Count: 0")

    def display_processed_frame(self):
        if self.cap is None or self.paused:
            return

        ret, frame = self.cap.read()

        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            return

        # Get the current count before calling detect_people
        current_count = self.people_counter_count

        frame, count = peoplecounter.detect_people(frame, current_count)  # Update to receive count from detect_people
        self.people_counter_count = count

        self.display_count = self.total_final_count + self.people_counter_count

        self.count_label.config(text=f"Final Count: {self.display_count}")

        frame = cv2.resize(frame, (1024, 576))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        img = Image.fromarray(frame)
        imgtk = ImageTk.PhotoImage(image=img)

        self.processed_frame_label.config(image=imgtk)
        self.processed_frame_label.imgtk = imgtk

        self.processed_frame_label.after(10, self.display_processed_frame)

def playback_content(page_container):
    playback_page = PlaybackPage(page_container)
    return playback_page
